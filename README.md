Comet
===

Features
---

A modern, single-file, header-only tuple library.
It's namesake feature is that is uses composition over inheritance to guarantee that the members are stored in continuous memory with zero overhead, while maintaining the intended order.
This allows for lots and lots of funny pointer magic.


Another amazing feature is the ability to name the tuple elements and access them by those names.
This means you don't have to remember the index of a certain element anymore (or use tedious enums, etc).
It is a pseudo struct, basically.
All of this comes, again, with zero overhead on memory.
And the best thing is, that all this happens at compile time.

However, the world is unfortunately not quite ready for these named tuples yet. (I.e. most compilers just don't support some specific type deductions from constructor parameters).
The named tuples have thus been delegated to the dev branch.
They work fine using whatever g++-10 ships with Ubuntu 21.04 by default for example, but not the one that can be installed under Ubuntu 20.04.
Older compilers than that might even have trouble corectly identifying typenames in some cases.


And one more feature, available already on the master branch:
The contents of the tuples are accessed by member functions.
This means you no longer have to think backwards when accessing your tuple's contents.
Just Compare: 
```
std::get<2>(foo).bar();
foo.get<2>().bar();
```

```
std::tuple_element<2>(foo)
foo.type<2>
```

It's really almost a struct.  
(The catch is, that in some cases where template parameters are used, you will have to type `foo.template get<N>().bar();`)



Dependencies
---
You just need a modern C++ compiler with the standard library (of which we use hardly any functionality really).  
Also, if you want to use cmake to build the example, you need that.  
That's it.  
(Well, I assume you also used git to clone this project.)



How to Build
---

Firstly, you need, at the very least, a C++17 capable compiler.
I recommend g++-10, but have also successfully tested compilation of the example under g++-9.
However, as with the named tuples issue, a different version of the same compiler might still cause issues.
(I'd have to investigate the standard to find out whether it's the compiler's fault or mine, but I can't be bothered.)

The build (of the example) is quite simple. 
I recommend using cmake and to build in a seperate directory like follows:

```
mkdir build
cd build
cmake ..
make
```


Or if you want to avoid using cmake:

```
mkdir build
cd build
g++ -std=c++20 ../main.cpp
```




Files
===

Tuple.h
---
This file is the entire library.
It contains all declarations and the definitions are inline as well. (constexpr even)


main.h
---
An example of how to actually use to Tuple struct.
It's dead simple.


CMakeLists.txt
---
A minimalistic configuration to build with cmake.
(If you can even call that a configuration.)


LICENSE
---
Duh.


README.md
---
This file.

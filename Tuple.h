#pragma once
#include <cstddef>
#include <utility>


namespace HIDDEN
{
    //template<size_t N, typename T0, typename... T>
    //using pick_type = typename std::conditional<!N, T0, pick_type<N-1,T...>>::type;

    template<size_t N, typename T0, typename... T> struct pts;

    template<typename T0, typename... T>
    struct pts<0, T0, T...>
    {
        using type = T0;
    };

    template<size_t N, typename T0, typename... T> 
    struct pts
    {
        using type = typename pts<N-1,T...>::type;
    };

    template<size_t N, typename... T>
    using pick_type = typename pts<N,T...>::type;




    template<typename T0, typename T1>
    struct iss 
    {
        static constexpr bool value = false;
    };

    template<typename T>
    struct iss<T, T>
    {
        static constexpr bool value = true;
    };

    template<typename T0, typename T1>
    constexpr bool is_same = iss<T0,T1>::value;
}



template<typename... T> struct Tuple;

template<typename T0>
struct Tuple<T0>
{
    T0 member;


	constexpr Tuple() = default;
	constexpr Tuple(const Tuple& in) = default;
	constexpr Tuple(Tuple&& in) = default;
	constexpr Tuple& operator=(const Tuple& in) = default;
	constexpr Tuple& operator=(Tuple&& in) = default;
	constexpr Tuple(T0&& t0);

    template<size_t N> using type = HIDDEN::pick_type<N, T0>;

    template<size_t N> constexpr type<N>& get();
    template<size_t N> constexpr const type<N>& get() const;

    template<typename S> constexpr S& get();
    template<typename S> constexpr const S& get() const;

    template<typename S> constexpr size_t find() const;


    static constexpr size_t size();
};

template<typename T0, typename... T> 
struct Tuple<T0,T...>
{
    T0 member;
    Tuple<T...> others;


	constexpr Tuple() = default;
	constexpr Tuple(const Tuple& in) = default;
	constexpr Tuple(Tuple&& in) = default;
	constexpr Tuple& operator=(const Tuple& in) = default;
	constexpr Tuple& operator=(Tuple&& in) = default;
	constexpr Tuple(T0&& t0, T&&... t);

    template<size_t N> using type = HIDDEN::pick_type<N, T0, T...>;

    template<size_t N> constexpr type<N>& get();
    template<size_t N> constexpr const type<N>& get() const;

    template<typename S> constexpr S& get();
    template<typename S> constexpr const S& get() const;

    template<typename S, size_t N=0> constexpr size_t find() const;

    static constexpr size_t size();
};


template<typename T0>
constexpr Tuple<T0>::Tuple(T0&& t0): member(std::forward<T0>(t0))
{
	
}

template<typename T0> template<size_t N> 
constexpr typename Tuple<T0>::template type<N>& Tuple<T0>::get()
{
    static_assert(!N, "Index greater than tuple size.");
    return member;
}

template<typename T0> template<size_t N> 
constexpr const typename Tuple<T0>::template type<N>& Tuple<T0>::get() const
{
    static_assert(!N, "Index greater than tuple size.");
    return member;
}

template<typename T0> template<typename S> 
constexpr S& Tuple<T0>::get()
{
    static_assert(HIDDEN::is_same<T0,S>, "Type not in tuple."); 
    return member;
}

template<typename T0> template<typename S> 
constexpr const S& Tuple<T0>::get() const
{
    static_assert(HIDDEN::is_same<T0,S>, "Type not in tuple."); 
    return member;
}

template<typename T0> template<typename S> 
constexpr size_t Tuple<T0>::find() const
{
    static_assert(HIDDEN::is_same<T0,S>, "Type not in tuple."); 
    return 0;
}

template<typename T0>
constexpr size_t Tuple<T0>::size()
{
    return 1;
}


template<typename T0, typename... T>
constexpr Tuple<T0, T...>::Tuple(T0&& t0, T&&... t): member(std::forward<T0>(t0)), others(std::forward<T>(t)...)
{
	
}

template<typename T0, typename... T> template<size_t N> 
constexpr typename Tuple<T0, T...>::template type<N>& Tuple<T0, T...>::get()
{
    if constexpr(!N)
    {
        return member;
    }
    else
    {
        return others.template get<N-1>();
    }
}

template<typename T0, typename... T> template<size_t N> 
constexpr const typename Tuple<T0, T...>::template type<N>& Tuple<T0, T...>::get() const
{
    if constexpr(!N)
    {
        return member;
    }
    else
    {
        return others.template get<N-1>();
    }
}

template<typename T0, typename... T> template<typename S>
constexpr S& Tuple<T0, T...>::get()
{
    if constexpr(HIDDEN::is_same<T0,S>)
    {
        return member;
    }
    else
    {
        return others.template get<S>();
    }
}

template<typename T0, typename... T> template<typename S>
constexpr const S& Tuple<T0, T...>::get() const
{
    if constexpr(HIDDEN::is_same<T0,S>)
    {
        return member;
    }
    else
    {
        return others.template get<S>();
    }
}

template<typename T0, typename... T> template<typename S, size_t N>
constexpr size_t Tuple<T0,T...>::find() const
{
    if constexpr(HIDDEN::is_same<HIDDEN::pick_type<N,T0,T...>, S>)
    {
        return N;
    }
    else
    {
        return find<S,N+1>();
    }
}

template<typename T0, typename... T>
constexpr size_t Tuple<T0,T...>::size()
{
    return 1 + sizeof...(T);
}



template<size_t N=0, typename STREAMTYPE, typename... T>
constexpr void print_tuple(STREAMTYPE& stream, const Tuple<T...>& tuple)
{
    typename Tuple<T...>::template type<N> element = tuple.template get<N>();

    if constexpr(!N)
    {
        stream<<"<";
    }
    else
    {
        stream<<", ";
    }

    stream<<element;

    if constexpr(N+1 < tuple.size())
    {
        print_tuple<N+1>(stream, tuple);
    }
    else
    {
        stream<<">";
    }
}

template<typename STREAMTYPE, typename... T>
constexpr STREAMTYPE& operator<<(STREAMTYPE& stream, const Tuple<T...>& tuple)
{
    print_tuple(stream, tuple);
    return stream;
}

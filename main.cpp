#include "Tuple.h"
#include <iostream>
#include <string>



//A super simple function that demonstrates how to loop through tuples using recursion.
template<size_t N=0, typename TUP>
void print_tuple(TUP tuple)
{
    std::cout<<tuple.template get<N>()<<"\t";
    if constexpr(N+1 < TUP::size())
    {
        print_tuple<N+1>(tuple);
    }
}



int main()
{
    //Let's define a tuple and set its elements.
    //Note, that it does not yet support initializer lists.
    Tuple<int, double, char, std::string> tup;
    tup.get<0>() = 1;
    tup.get<1>() = 2.2;
    tup.get<2>() = '3';
    tup.get<3>() = "four";

    //we can print tuples with the examplary function, defined above
    print_tuple(tup);
    std::cout<<"\n";

    //or we can use the much better inbuilt operator<< overload
    std::cout<<tup<<"\n";


    //we can also change entries by querying for type
    tup.get<double>() = 5.5;
    tup.get<tup.find<char>()>() = '6';
    std::cout<<tup<<"\n";


	Tuple<int,double,std::string> tup2(7,8.8,"nin");
	std::cout<<tup2<<"\n";



    return 0;
}
